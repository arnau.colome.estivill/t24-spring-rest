DROP table IF EXISTS empleado;

create table empleado(
	id int auto_increment PRIMARY KEY,
	nombre varchar(250),
	trabajo enum('Profesor','Psicologa','Programador', 'Constructor'),
	salario integer
);

insert into empleado (nombre, trabajo, salario)values('Jose', 'Profesor', '2334'), ('Laura', 'Psicologa', '1544'), ('Marc', 'Programador', '3345'), ('David', 'Programador', '1125'), ('Pedro', 'Constructor', '3544');
