package com.crud.h2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T24CrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(T24CrudApplication.class, args);
	}

}
