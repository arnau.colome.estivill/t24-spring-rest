package com.crud.h2.dto;

import javax.persistence.*;

@Entity
@Table(name="empleado")//en caso que la tabala sea diferente
public class Empleado {

	//Atributos de entidad empleado
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private Long id;
	private String nombre;
	private String trabajo;
	private int salario;

	//Constructores
	
	public Empleado() {
	
	}

	/**
	 * @param id
	 * @param nombre
	 * @param trabajo	
	 * @param salario
	 */
	public Empleado(Long id, String nombre, String trabajo, int salario) {
		//super();
		this.id = id;
		this.nombre = nombre;
		this.trabajo = trabajo;
		this.salario = puestoTrabajo(trabajo, salario);
	}

	public int puestoTrabajo(String trabajo, int salario) {
		switch(trabajo) {
			case "Profesor":
				salario=2030;
				break;
			case "Constructor":
				salario=1500;
				break;
			case "Programador":
				salario=1990;
				break;
			case "Psicologa":
				salario=2510;
				break;
		}
		return salario;
	}


	//Getters y Setters
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the trabajo
	 */
	public String getTrabajo() {
		return trabajo;
	}

	/**
	 * @param trabajo the trabajo to set
	 */
	public void setTrabajo(String trabajo) {
		this.trabajo = trabajo;
		this.salario=puestoTrabajo(trabajo, salario);
	}

	/**
	 * @return the salario
	 */
	public int getSalario() {
		return salario;
	}

	/**
	 * @param salario the salario to set
	 */
	public void setSalario(int salario) {
		this.salario = salario;
	}
	

	@Override
	public String toString() {
		return "Empleado [id=" + id + ", nombre=" + nombre + ", trabajo=" + trabajo	+ ", salario=" + salario + "]";
	}

	
}
